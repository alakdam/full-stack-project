import React from "react";
import Dashboard from "./components/Dashboard/Dashboard";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import UpdateStudent from "./components/UpdateStudent/UpdateStudent";
import Datatable from "./components/Datatable/Datatable";
function App() {
  return (
    <div>
      <Router>
        <Dashboard />
        <Switch>
          <Route exact path="/students" component={Datatable} />
          <Route path="/edit/:id" component={UpdateStudent} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
