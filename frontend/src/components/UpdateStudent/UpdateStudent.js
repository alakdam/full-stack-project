import React, { useState, useEffect } from "react";
import { updateStudent, singleStudent } from "../Api/Api";

function UpdateStudents() {
  const [student, setStudent] = useState({
    name: "",
    birthday: "",
    address: "",
    zipcode: "",
    city: "",
    phone: "",
    email: ""
  });

  return (
    <React.Fragment>
      <div className="app">
        <div className="row">
          <form className="col s12">
            <div className="row">
              <div className="input-field col s3">
                <input id="name" type="text" data-length="4" />
                <label htmlFor="input_text">Name</label>
              </div>
            </div>
            <div className="col">
              <div className="input-field col s12">
                <input id="birthday" type="date" data-length="4" />
                <label htmlFor="input_text">Date of birth</label>
              </div>
            </div>
            <div className="row">
              <div className="input-field col s3">
                <input id="address" type="text" data-length="4" />
                <label htmlFor="input_text">Address</label>
              </div>
            </div>
            <div className="col">
              <div className="input-field col s12">
                <input id="zipcode" type="number" data-length="4" />
                <label htmlFor="input_text">Zipcode</label>
              </div>
            </div>
            <div className="row">
              <div className="input-field col s3">
                <input id="city" type="text" data-length="4" />
                <label htmlFor="input_text">City</label>
              </div>
            </div>
            <div className="col">
              <div className="input-field col s12">
                <input id="phone" type="tel" data-length="4" />
                <label htmlFor="input_text">Phone</label>
              </div>
            </div>
            <div className="row">
              <div className="input-field col s3">
                <input id="email" type="text" data-length="4" />
                <label htmlFor="input_text">Email</label>
              </div>
            </div>
            <button
              className="btn waves-effect blue lighten-1"
              type="submit"
              name="action"
            >
              update
            </button>
          </form>
        </div>
      </div>
    </React.Fragment>
  );
}

export default UpdateStudents;
