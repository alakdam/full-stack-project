import React, { useState, useEffect } from "react";
import { logEntry, deleteStudent, updateStudent } from "../Api/Api";
import { Link } from "react-router-dom";
import UpdateStudents from "../UpdateStudent/UpdateStudent";

function Datatable() {
  const [total, settotal] = useState([]);
  const [searchItem, setsearchItem] = useState({
    item: ""
  });
  const [data, setdata] = useState([]);
  const handleChange = e => {
    setsearchItem({ item: e.target.value });
  };

  const getEntries = async () => {
    const logEntries = await logEntry();

    //console.log(logEntries);
    settotal(logEntries.count);
    setdata(logEntries.students);
  };

  const deleteData = async id => {
    //console.log(id);
    await deleteStudent(id);
  };

  const update = async id => {
    console.log(id);
    await updateStudent(id);
  };
  useEffect(() => {
    getEntries();
  }, []);

  return (
    <div>
      <div style={{ paddingLeft: "800px" }}>
        <input
          placeholder="Search student"
          onChange={e => handleChange(e)}
          style={{ width: "200px", height: "30px" }}
          value={searchItem.item}
        />
      </div>
      <p>Total student: {total} </p>
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>City</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Date of birth</th>
            <th>Zipcode</th>
          </tr>
        </thead>

        <tbody>
          {data
            .filter(student => {
              return student.name
                .toLowerCase()
                .includes(searchItem.item.toLowerCase());
            })
            .map(student => {
              return (
                <tr>
                  <td>{student.name}</td>
                  <td>{student.city}</td>
                  <td>{student.address}</td>
                  <td>{student.phone}</td>
                  <td>{student.email}</td>
                  <td>{student.birthday}</td>
                  <td>{student.zipcode}</td>
                  <td>
                    <a
                      className="waves-effect red btn-small"
                      onClick={() => deleteData(student.id)}
                    >
                      Delete
                    </a>
                  </td>
                  <td>
                    <Link
                      to={"/edit/" + student.id}
                      className="btn btn-primary"
                    >
                      Edit
                    </Link>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
}

export default Datatable;
