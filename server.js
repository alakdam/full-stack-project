require("dotenv").config();
const express = require("express");
const app = express();
const morgan = require("morgan");
const helmet = require("helmet");
const cors = require("cors");

const { Student, Course } = require("./db");
//app middlewear

app.use(morgan("common"));
app.use(helmet());
app.use(cors());
app.use(express.json()); //body Parser

//student

app.get("/students", async (req, res, next) => {
  try {
    await Student.findAll({
      include: [
        {
          model: Course
        }
      ]
    }).then(docs => {
      const response = {
        count: docs.length,
        students: docs
      };
      res.json(response);
    });
  } catch (error) {
    console.log(error);
  }
});

app.get("/students/:id", async (req, res, next) => {
  const id = req.params.id;
  try {
    Student.findByPk(id).then(data => {
      console.log(data);
      res.json(data);
    });
  } catch (error) {
    console.log(error);
  }
});

app.put("/students/:id", async (req, res) => {
  const id = req.params.id;
  const update = req.body;
  try {
    await Student.update(update, { where: { id } }).then(data => {
      res.json(data);
    });
  } catch (error) {
    console.log(error);
  }
});

app.delete("/students/:id", async (req, res, next) => {
  const id = req.params.id;

  try {
    Student.destroy({ where: { id } }).then(data => {
      res.json(data);
    });
  } catch (error) {
    console.log(error);
  }
});

app.post("/students", async (req, res, next) => {
  try {
    const logs = new Student(req.body);
    const entry = await logs.save();
    res.json(entry);
  } catch (error) {
    if (error.name === "ValidationError") {
      res.status(422);
    }
    next(error);
  }
});

//course

app.get("/courses", async (req, res, next) => {
  try {
    await Course.findAll().then(docs => {
      const response = {
        count: docs.length,
        courses: docs
      };
      res.json(response);
    });
  } catch (error) {
    console.log(error);
  }
});

app.get("/courses/:id", async (req, res, next) => {
  const id = req.params.id;
  try {
    Course.findByPk(id).then(data => {
      console.log(data);
      res.json(data);
    });
  } catch (error) {
    console.log(error);
  }
});

app.put("/courses/:id", async (req, res, next) => {
  const id = req.params.id;
  const update = req.body;
  try {
    await Course.update(update, { where: { id } }).then(data => {
      res.json(data);
    });
  } catch (error) {
    console.log(error);
  }
});

app.delete("/courses/:id", async (req, res, next) => {
  const id = req.params.id;

  try {
    Course.destroy({ where: { id } }).then(data => {
      res.json(data);
    });
  } catch (error) {
    console.log(error);
  }
});

app.post("/courses", async (req, res, next) => {
  try {
    const logs = new Course(req.body);
    const entry = await logs.save();
    res.json(entry);
  } catch (error) {
    if (error.name === "ValidationError") {
      res.status(422);
    }
    next(error);
  }
});

const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`🚀 App is listening at port ${port}!`));
